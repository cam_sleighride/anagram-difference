package pl.kamilkulig;

import java.util.HashMap;
import java.util.Map;

public class Main {

    public static int[] getMinimumDifference(String[] a, String[] b) {
        int[] results = new int[a.length];

        for (int i = 0; i < a.length; i++) {
            results[i] = areAnagrams(a[i], b[i]);
        }

        return results;
    }

    public static int areAnagrams(String one, String two) {
        if (one.length() == two.length()) {
            int counter = 0;
            char[] oneTable = one.toCharArray();
            char[] twoTable = two.toCharArray();

            Map<Character, Integer> lettersInWord1 = new HashMap<Character, Integer>();
            for (char c : oneTable) {
                int count = 1;
                if (lettersInWord1.containsKey(c)) {
                    count = lettersInWord1.get(c) + 1;
                }
                lettersInWord1.put(c, count);
            }

            for (char c : twoTable) {
                int count;
                if (lettersInWord1.containsKey(c) && lettersInWord1.get(c) != 0) {
                    count = lettersInWord1.get(c) - 1;
                    lettersInWord1.put(c, count);
                }
            }

            for (char c : lettersInWord1.keySet()) {
                if (lettersInWord1.get(c) != 0) {
                    counter += lettersInWord1.get(c);
                }
            }

            if (counter != 0) {
                return counter;
            }

            return 0;

        } else {
            return -1;
        }
    }

    public static void show(int[] c) {
        for (int i : c) {
            System.out.println("" + i);
        }
        System.out.println();
    }

    public static void main(String[] args) {

        String[] a = new String[]{"aa", "b"};
        String[] b = new String[]{"aa", "a"};
        int[] c = getMinimumDifference(a, b);
        show(c);

        String[] a2 = new String[]{"a", "jk", "abb", "mn", "abc"};
        String[] b2 = new String[]{"bb", "kj", "bbc", "op", "def"};
        c = getMinimumDifference(a2, b2);
        show(c);

        String[] a3 = new String[]{"abc", "aaa", "aacc"};
        String[] b3 = new String[]{"bba", "bbb", "bbdd"};
        c = getMinimumDifference(a3, b3);
        show(c);

        String[] a4 = new String[]{"adbop"};
        String[] b4 = new String[]{"zogpc"};
        c = getMinimumDifference(a4, b4);
        show(c);
    }
}
