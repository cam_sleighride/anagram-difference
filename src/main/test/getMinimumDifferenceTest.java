import org.junit.Test;
import pl.kamilkulig.Main;

import static org.junit.Assert.assertArrayEquals;

public class getMinimumDifferenceTest {

    @Test
    public void firstTest() {
        String[] a0 = new String[]{"ab"};
        String[] b0 = new String[]{"bc"};

        String[] a1 = new String[]{"aa", "b"};
        String[] b1 = new String[]{"aa", "a"};

        String[] a2 = new String[]{"a", "jk", "abb", "mn", "abc"};
        String[] b2 = new String[]{"bb", "kj", "bbc", "op", "def"};

        String[] a3 = new String[]{"abc", "aaa"};
        String[] b3 = new String[]{"bba", "bbb"};

        assertArrayEquals("1", Main.getMinimumDifference(a0, b0), new int[]{1});

        assertArrayEquals("2", Main.getMinimumDifference(a1, b1), new int[]{0, 1});

        assertArrayEquals("3", Main.getMinimumDifference(a2, b2), new int[]{-1, 0, 1, 2, 3});

        assertArrayEquals("4", Main.getMinimumDifference(a3, b3), new int[]{1, 3});

    }
}